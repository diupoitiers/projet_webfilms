 if [ ! -d "venv" ]; then
   echo "Run init.sh first"
   exit 1
fi
source venv/bin/activate
export FLASK_APP=webfilms.py
export FLASK_ENV=development
python $FLASK_APP

