# Notes

- 2022/05/04 : Ajouté utilisaiton d'un environnement virtuel. Créer cet environnement en exécutant `./init.sh`. Le projet fonctionne bien avec Flask en version 2.1.X
- 2021/07/02 : `flask` est sorti en version 2 il y a peu. Je n'ai pas encore testé le projet avec la dernière version de `flask` (le projet utilise actuellement la version 1.1)

# Manuel

Pour lancer le site Web : `run.sh` (adaptation nécessaire pour Windows, mais
ce doit être facile). Une fois le serveur lancé, ouvrir un navigateur qui
pointe à l'adresse : <http://localhost:5000>. Le projet est configuré en mode debug,
le traceback s'affiche dans le navigateur, et si un fichier est modifié, le
serveur est relancé. C'est la configuration normale pour le développement, mais pas
pour la mise en production.

Le module flask doit être installé  (`pip install flask`) pour que ça fonctionne.

Cet exemple illustre quelques unes des possibilités de `flask` et de Bulma CSS pour la mise
en page. `flask` permet de faire beaucoup plus que ce qui est montré dans cet exemple, et de
bien d'autres façons. Nous pensons toutefois que cet exemple constitue une base propre pour
un projet élève.

Le projet ne contient pas de Javascript, pas de requêtes Ajax etc... juste des
pages Web dynamiques générées avec flask, des templates HTML et du CSS (Bulma).

À noter qu'il existe des alternatives à flask, par exemple : bottle, web.py ou
django (ce dernier est le plus connu et le plus utilisé, mais probablement trop
complet pour un usage avec les élèves).

Documentation de flask : <https://flask.palletsprojects.com/en/1.1.x/>

Vidéo explicative ici : <https://videotheque.univ-poitiers.fr/vid/constuctionreq/> 

## Screenshots 

![](imgs/cap01.png)

![](imgs/cap02.png)



